# S1 LRFs for ROOT
This is a minimal project to get started with using S1 LRFs for S1-only position reconstruction and S1 light collection efficiency corrections. Current implementation does not use individual LRFs. Instead, neighboring PMTs are joined in clusters of 4 to 7 units and LRFs are calculated per cluster, i.e. for the sums of S1 areas of PMTs in each cluster. See [these slides](https://docs.google.com/presentation/d/1PXXScUnVSzsLIkUbgWo2LZoQpgC6zQ8h2y2BuFEY6wY) for details and preliminary performance plots.


## Getting started

### Pre-requisits
* ROOT with Minuit2 support enabled (check with `root-config --has-minuit2`)
* eigen3 linear algebra library

### Compiling Mercury libs
Clone [Mercury GitHib repo](https://github.com/vovasolo/pymercury). You may need to adjust include paths and shell commands in the Makefile according to your system configuration. You may also need to run `thisroot.sh` or equivalent to create the correct evironment for ROOT commands. Now build the Mercury shared libraries with 
```
make liblrm
make libmercury
```
If all goes well, `liblrm.so` and `libmercury.so` files will appear in the Mercury project folder. Copy them over to this (i.e. **S1 LRFs for ROOT**) project folder.

### Testing S1-only reconstruction
Start ROOT from this project folder and run the following commands:
```
.include ./include
.include /usr/include/eigen3
.L Reconstruction.C
gSystem->Load("liblrm.so");
gSystem->Load("libmercury.so");
process("Alphas1000.root", "out.txt")
```
The last command processes first 10 events from `Alphas1000.root` and output the position and drift time reconstructed from S1 signals as well as the S2 position and the measured drift time. For example:
```
=== Event #0 ===
s2x = 657.22
s2y = 32.1124
dtime = 874.65
s1x = 649.261
s1y = 37.3735
s1_dtime = 874.692
Reduced chi2 = 1.57163
S1 sum: 53911.3
S1 corrected sum: 46198.8
```
*Side note: in my experience, ROOT internal compiler is easily screwed up, so if you get a bunch of strange compilation errors, try restarting ROOT application first and see if they gone.*